<?php
/**
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

return array(
	'name' => '{APPLICATION NAME}',
	'preload' => array('log'),
	'aliases' => array(
		'frontend' => dirname(__FILE__) . '/../..' . '/frontend',
		'common' => dirname(__FILE__) . '/../..' . '/common',
		'backend' => dirname(__FILE__) . '/../..' . '/backend',
		'vendor' => dirname(__FILE__) . '/../..' . '/common/lib/vendor',
	),
	'import' => array(
		'common.extensions.components.*',
		'common.components.*',
		'common.helpers.*',
		'common.models.*',
		'common.modules.user.models.*',
		'common.modules.user.components.*',
		'application.components.*',
		'application.controllers.*',
		'application.extensions.*',
		'application.helpers.*',
		'application.models.*'
	),
	'modules'=>array(
    	'user'=>array(
    		'class' => 'common.modules.user.UserModule',

        	'tableUsers' => 'user',
			'tableProfiles' => 'user_profiles',
			'tableProfileFields' => 'user_profile_fields',

         	# automatically login from registration
        	'autoLogin' => false,
        ),

    ),
	'components' => array(
		'db'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=DLMS',
			'username' => 'dlms_user',
			'password' => 'dlms1234',
			'charset' => 'utf8',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class'  => 'CLogRouter',
			'routes' => array(
			 	array(
                	'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
/*
				array(
					'class'        => 'CDbLogRoute',
					'connectionID' => 'db',
					'levels'       => 'error, warning',
				),
*/
			),
		),
	),
	// application parameters
	'params' => array(
		// php configuration
		'php.defaultCharset' => 'utf-8',
		'php.timezone'       => 'UTC',
	),
);