<?php
/**
 * backend.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'backend');

// web application configuration
return array(
	'basePath' => realPath(__DIR__ . '/..'),
	// import paths
    'import' => array(
    	'bootstrap.helpers.TbHtml',
    ),
	// path aliases
	'aliases' => array(
		// assumed the use of yiistrap and yiiwheels extensions
		'bootstrap' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiistrap',
		'yiiwheels' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiiwheels'
	),
	//'theme'=>'abound',
	// application behaviors
	'behaviors' => array(
//		'onBeginRequest' => array(
//       		'class' => 'user.behaviors.RequireLogin',
//    	),
	),

	// controllers mappings
	'controllerMap' => array(),

	// application modules
	'modules' => array(
		'auth' => array(
			'class' => 'common.modules.user.auth.AuthModule',
			'userNameColumn' => 'username',
			'admins' => array('admin'), // users with full access
		),

	),

	// application components
	'components' => array(

		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),

		'clientScript' => array(
			'scriptMap' => array(
				'bootstrap.min.css' => false,
				'bootstrap.min.js' => false,
				'bootstrap-yii.css' => false,
				'jquery.js' => '/js/libs/jquery-1.9.1.min.js',
				'jquery.min.js' => '/js/libs/jquery-1.9.1.min.js',
			)
		),
		'urlManager' => array(
			// uncomment the following if you have enabled Apache's Rewrite module.
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array(
				// default rules
				'<controller:\w+>/<id:\d+>' 				=> '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' 	=> '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'				=> '<controller>/<action>',
			),
		),
		'user' => array(
			'class'=>'common.modules.user.components.WebUser',
		),
		'authManager' => array(
            'behaviors' => array(
				'auth' => array(
					'class' => 'auth.components.AuthBehavior',
				),
			),
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		)
	),
);